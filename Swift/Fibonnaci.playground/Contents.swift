//: Playground - noun: a place where people can play

import UIKit
import Foundation

let goldanRatio : Double = 1.618034

func fibonacci (n: Int) -> Int {
    // Write your code here.
    let result : Int = Int((pow(goldanRatio,Double(n)) - pow((1 - goldanRatio),Double(n)))/5.squareRoot())
    return result
}

// read the integer n
let n = Int(readLine()!)!
let result = fibonacci(n: n)
print(result)

// print the nth fibonacci number
// print(fibonacci(n: n))
